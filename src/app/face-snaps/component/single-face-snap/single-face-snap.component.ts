import {Component, Input, OnInit} from '@angular/core';
import {FaceSnap} from "../../../core/models/face-snap.model";
import {FaceSnapsService} from "../../../core/services/face-snaps.service";
import {ActivatedRoute} from "@angular/router";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-single-face-snap',
  templateUrl: './single-face-snap.component.html',
  styleUrls: ['./single-face-snap.component.scss']
})
export class SingleFaceSnapComponent implements OnInit {
 // @Input() faceSnap!: FaceSnap;
  faceSnap$!: Observable<FaceSnap>;
  buttonText!: string;

  constructor(private faceSnapService: FaceSnapsService,
              private route: ActivatedRoute) {
  }
  ngOnInit() {
    this.buttonText = 'Oh Snaps!';
    const faceSnapId = +this.route.snapshot.params['id'];
    this.faceSnap$ = this.faceSnapService.getFaceSnapById(faceSnapId);
  }

  onSnap(id: number) {
    if (this.buttonText === 'Oh Snaps!') {
      this.faceSnap$ = this.faceSnapService.snapFaceSnapById(id, 'snap').pipe(
          tap(() => this.buttonText = 'Oups Snap!')
      );
    } else {
      this.faceSnap$ = this.faceSnapService.snapFaceSnapById(id, 'unsnap').pipe(
          tap(() => this.buttonText = 'Oh Snaps!')
      );
    }
  }
}
